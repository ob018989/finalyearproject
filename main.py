#Library imports
import pandas as pd
import streamlit as st
import tweepy
from PIL import Image

st.set_page_config(page_title=None, page_icon=None, layout="wide", initial_sidebar_state="auto", menu_items=None)
@st.cache
def convert_df(df):
   return df.to_csv().encode('utf-8')
#st.set_page_config(layout="wide")
if 'Counter' not in st.session_state:
    st.session_state['Counter'] = 0
if 'Question' not in st.session_state:
    st.session_state['Question'] = "No Question"
if 'score' not in st.session_state:
    st.session_state['score'] = 0
if 'sidebar_ratio' not in st.session_state:
    st.session_state['sidebar_ratio'] = "Extract tweets"

#menu implementation

with st.sidebar:
    add_radio = st.radio("MENU",["Sentiment Analysis Dashboard", "Extract tweets",
                                 "Depression Self-assessment"], key='sidebar_ratio')

#api_key = st.text_input('Twitter API Key', 'F1aKIv54mAIMon4lSfo9pWmlT')
#api_secret_key = st.text_input('Twitter API Secret Key', 'KLDtNSq5rDEMSfPWJVKYzbgte26raIhb86mR7Z3yyOhbkdDIdJ')
#access_token = st.text_input('Twitter Access Token', '1484196974798643206-cC08BZ3eTHuoCqUR4rdC9yKbslkXFr')
#access_token_secret = st.text_input('Twitter Access Token Secret', 'emzp02IodFWOiP7txIBko0TnXHosGkP23YiuRmB3LTMU1')

api_key = 'F1aKIv54mAIMon4lSfo9pWmlT'
api_secret_key = 'KLDtNSq5rDEMSfPWJVKYzbgte26raIhb86mR7Z3yyOhbkdDIdJ'
access_token = '1484196974798643206-cC08BZ3eTHuoCqUR4rdC9yKbslkXFr'
access_token_secret = 'emzp02IodFWOiP7txIBko0TnXHosGkP23YiuRmB3LTMU1'

search_words = ""  # enter your words

auth = tweepy.OAuthHandler(api_key, api_secret_key)
auth.set_access_token(access_token, access_token_secret)
api = tweepy.API(auth,wait_on_rate_limit=True)

#sentiment analyses graphs implementation

if st.session_state['sidebar_ratio'] == 'Sentiment Analysis Dashboard':
    st.title("Sentiment Analysis Dashboard")
    st.markdown('On this page, you can find the Sentiment Analysis of Depressed Tweets, conducted in Jupyter Notebook. '
                'Also, tweet comparison analysis on three health accounts.')
    image = Image.open('first-1.png')
    image2 = Image.open('second-1.png')
    st.image(image, caption="Sentiment Analysis of Tweets", width=None, use_column_width=None, clamp=False,
             channels="RGB", output_format="auto")
    st.image(image2, caption="Tweet Comparison", width=None, use_column_width=None, clamp=False,
             channels="RGB", output_format="auto")

#Tweet extraction implementation


elif st.session_state['sidebar_ratio'] == 'Extract tweets':
    st.title('Tweets Extractor ')
    st.markdown('On this page, you can extract tweets. Just search any keyword and to search a specific account enter'
                ' - from:"account name" "keyword"')
    tweets = []
    entry = st.text_input('Enter keyword:')
    extractbutton = st.button("Extract tweets")
    search_words += entry
    new_search = search_words + " -filter:retweets"
    if extractbutton:
        if len(entry) == 0:
            st.warning("Please enter at least one keyword")
        else:
            for tweet in tweepy.Cursor(api.search_tweets, q=new_search, count=1,
                                           lang="en",
                                           since_id=0).items(20):
                tweets += [[tweet.created_at, tweet.text, tweet.user.screen_name, tweet.user.location]]
            st.subheader('Tweets')
            try :
                tweets = pd.DataFrame(tweets)
                tweets.columns = ["Date", "Tweet", "User", "Location"]
                st.table(tweets)
                csv = convert_df(tweets)
                st.download_button(
                        "Press to Download",
                        csv,
                        "file.csv",
                        "text/csv",
                        key='download-csv'
                )
            except:
                st.warning("Sorry, no tweets from the last 7 days match your keywords.")


#test implementation

else:
    st.title('Self-Assessment Depression Test')
    st.markdown("Tool to determine your state of Depression")
    container = st.empty()

    questions = ['Are you having trouble falling asleep?', 'How often are you bothered by feeling tired or having little energy?',
                 'How often are you bothered by poor appetite or overeating?',
                 'How often are you bothered by feeling bad about yourself,'
                 ' or that you are a failure, or have let yourself or other '
                 'people down?', 'How often do you have trouble concentrating '
                                 'on things in your daily activities?',
                 'Have you had an anxiety/panic attacks?', 'How often do you feel nervous, anxious or on edge?',
                 'How often are you bothered by feeling afraid about something awful might happen?',
                 'How often are you bothered by having trouble relaxing?',
                 'How often have you had little interest or pleasure in doing things?']
    if st.session_state['Counter'] < 9:
        container.write("Question n°: " + str(st.session_state['Counter']+1) + " " + questions[st.session_state['Counter']] )
        st.radio(label="Please Choose One", options=["Not at all", "Several days", "All the time"], key='answer')
        but = st.button("Confirm")
    else:
        if st.session_state["score"] < 10:
            text = ' You are not depressed 🥳 :rainbow: '
        elif st.session_state["score"] < 15:
            text = ' You are midly depressed :weary:'
        else:
            text = ' You are in complete depression :fearful: '
        st.markdown("Your score: " + str(st.session_state["score"]) + text)
        container.empty()
    try:
        if but:
                if st.session_state["answer"] == "Not at all":
                    pass
                elif st.session_state["answer"] == "Several days":
                    st.session_state["score"] += 1
                elif st.session_state["answer"] == "All the time":
                    st.session_state["score"] += 2
                st.session_state['Counter'] = st.session_state['Counter'] +1
                with container.container():
                    st.write("Question n°" + str(st.session_state['Counter']+1) + " " + questions[st.session_state['Counter']] )
    except:
        pass

    reset_button = st.button("Reset Test")
    if reset_button:
        st.session_state['Counter'] = 0
        st.session_state["score"] = 0

